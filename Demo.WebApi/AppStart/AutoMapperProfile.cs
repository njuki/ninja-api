﻿using AutoMapper;
using Demo.Common.DataTransferObjects;
using Demo.Common.Entities;

namespace Demo.WebApi.AppStart {
    public class AutoMapperProfile : Profile {
        public AutoMapperProfile() {
            CreateMap<RegisterUserRequestDto, User>();
        }
    }
}
