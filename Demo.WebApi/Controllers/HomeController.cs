﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace Demo.WebApi.Controllers {
    [Route("")]
    [ApiController]
    public class HomeController : ControllerBase {
       

        [HttpGet, Route("")]
        public async Task<IActionResult> Index() {
            DateTime now = DateTime.Now;
            // do absolutely nothing
            await Task.FromResult("");
            // calculate the time it takes to do nothing
            var spent = DateTime.Now.Subtract(now);
            return Ok($"API is alive.. {spent.TotalMilliseconds} milliseconds");
        }
    }

}
