﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Demo.WebApi.Controllers {
    [Route("information")]
    [ApiController]
    public class InformationController : ControllerBase {

        [HttpGet, Route("free")]
        public async Task<IActionResult> Free() {
            var information = await Task.FromResult("This information can be seen by anyone");
            return Ok(information);
        }

        [Authorize]
        [HttpGet, Route("protected")]
        public async Task<IActionResult> Protected() {
            var information = await Task.FromResult("Protected information for certified ninjas only");
            return Ok(information);
        }
    }

}
