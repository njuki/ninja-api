﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Demo.Business.Contracts;
using Demo.Common.DataTransferObjects;
using Demo.Common.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Demo.WebApi.Controllers {
    [Route("user")]
    [ApiController]
    public class UserController : ControllerBase {
        private readonly IUserService _userService;


        public UserController(IUserService userService) {
            _userService = userService;
        }


        [HttpPost("register")]
        public async Task<ActionResult<string>> RegisterUser([FromBody] RegisterUserRequestDto userRegistrationDto) {
            try {
                var loginResponse = await _userService.RegisterUser(userRegistrationDto);
                return Ok(loginResponse.JwtToken);
            } catch (InvalidDataException exception) {
                ModelState.AddModelError("Username", exception.Message);
                return this.ValidationProblem();
            } catch (Exception e) {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult<string>> LoginUser([FromBody] LoginUserRequestDto loginUserRequestDto) {
            try {
                var loginResponse = await _userService.LoginUser(loginUserRequestDto);
                if (loginResponse.JwtToken != null) {
                    return Ok(loginResponse.JwtToken);
                } else {
                    return StatusCode(StatusCodes.Status400BadRequest, "Invalid username or password");
                }
            } catch (Exception e) {
                return StatusCode(StatusCodes.Status400BadRequest, e.Message);
            }
        }

        [HttpGet("get/{username}")]
        public async Task<ActionResult<User>> GetUserByUsername(string username) {
            try {
                var data = await _userService.GetUserByUsername(username);
                return Ok(data);
            } catch (Exception e) {
                return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
            }

        }
    }

}
