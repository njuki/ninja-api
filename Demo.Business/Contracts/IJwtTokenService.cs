﻿using System;
using System.Collections.Generic;
using System.Text;
using Demo.Common.Entities;

namespace Demo.Business.Contracts {
    public interface IJwtTokenService {
        string GenerateToken(User user);

    }
}
