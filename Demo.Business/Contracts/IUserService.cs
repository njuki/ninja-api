﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.Common.DataTransferObjects;
using Demo.Common.Entities;

namespace Demo.Business.Contracts {
    public interface IUserService {
        Task<LoginUserResponseDto> RegisterUser(RegisterUserRequestDto registerUserRequestDto);
        Task<User> GetUserByUsername(string username);
        Task<LoginUserResponseDto> LoginUser(LoginUserRequestDto loginUserRequestDto);


    }
}
