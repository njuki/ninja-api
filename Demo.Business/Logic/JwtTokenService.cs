﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Demo.Business.Contracts;
using Demo.Common.Constants;
using Demo.Common.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Demo.Business.Logic {
    public class JwtTokenService : IJwtTokenService {
        private readonly IConfiguration _configuration;

        public JwtTokenService(IConfiguration configuration) {
            _configuration = configuration;
        }


        public string GenerateToken(User user) {
            int epoch = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            List<ClaimData> tokenDataList = new List<ClaimData>() {
                new ClaimData() { ClaimType =JwtTokenConstants.USER_ID_CLAIM, ClaimValue = user.Username, ClaimValueType =  ClaimValueTypes.String} ,
                new ClaimData() { ClaimType =JwtTokenConstants.USER_FIRST_NAME_CLAIM, ClaimValue = user.FirstName, ClaimValueType =  ClaimValueTypes.String} ,
                new ClaimData() { ClaimType =JwtTokenConstants.USER_FAMILY_NAME_CLAIM, ClaimValue = user.FamilyName, ClaimValueType =  ClaimValueTypes.String} ,
                new ClaimData() { ClaimType =JwtRegisteredClaimNames.Iat, ClaimValue = epoch.ToString(), ClaimValueType =  ClaimValueTypes.Integer}
            };
            return GenerateToken(tokenDataList);
        }

        private string GenerateToken(IEnumerable<ClaimData> claimDataList) {
            var claims = claimDataList.Select(c => new Claim(c.ClaimType, c.ClaimValue, c.ClaimValueType)).ToList();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration[JwtTokenConstants.TOKEN_KEY_NAME]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: JwtTokenConstants.TOKEN_VALID_ISSUER,
                audience: JwtTokenConstants.TOKEN_VALID_AUDIENCE,
                notBefore: DateTime.Now.AddMinutes(-5),
                claims: claims,
                expires: DateTime.Now.AddDays(5),
                signingCredentials: creds);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        private class ClaimData {
            public string ClaimType { get; set; }
            public string ClaimValueType { get; set; }
            public string ClaimValue { get; set; }
        }

    }
}
