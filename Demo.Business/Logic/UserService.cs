﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Authentication;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Demo.Business.Contracts;
using Demo.Common.DataTransferObjects;
using Demo.Common.Entities;
using Demo.Common.Persistence;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Demo.Business.Logic {
    public class UserService : IUserService {
        private readonly DemoContext _context;
        private readonly IMapper _mapper;
        private readonly IPasswordHasher<User> _hasher;
        private readonly IJwtTokenService _jwtTokenService;

        public UserService(DemoContext context, IMapper mapper, IPasswordHasher<User> hasher, IJwtTokenService jwtTokenService) {
            _context = context;
            _mapper = mapper;
            _hasher = hasher;
            _jwtTokenService = jwtTokenService;
        }

        public async Task<LoginUserResponseDto> RegisterUser(RegisterUserRequestDto registerUserRequestDto) {
            // API validates required fields already, no need to recheck them here

            var existingUser = await _context.Users.FirstOrDefaultAsync(u => u.Username.ToLower() == registerUserRequestDto.Username.ToLower());
            if (existingUser != null) {
                throw new InvalidDataException($"Username '{registerUserRequestDto.Username}' alredy exists");
            }

            var user = _mapper.Map<User>(registerUserRequestDto);
            user.PasswordHash = _hasher.HashPassword(user, registerUserRequestDto.Password);
            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            // upon successful register, just log in the user
            var returnValue = new LoginUserResponseDto {
                JwtToken = _jwtTokenService.GenerateToken(user)
            };

            return returnValue;
        }

        public async Task<User> GetUserByUsername(string username) {
            return await _context.Users.FirstOrDefaultAsync(u => u.Username == username);
        } 

        public async Task<LoginUserResponseDto> LoginUser(LoginUserRequestDto loginUserRequestDto) {
            var returnValue = new LoginUserResponseDto();
            User user = await _context.Users.FirstOrDefaultAsync(u => u.Username == loginUserRequestDto.Username);
            if (user == null || _hasher.VerifyHashedPassword(user, user.PasswordHash, loginUserRequestDto.Password) == PasswordVerificationResult.Failed) {
                returnValue.ErrorMessage = "Invalid username or password";
                return returnValue;
            }

            returnValue.JwtToken = _jwtTokenService.GenerateToken(user);
            return returnValue;

        }
    }
}
