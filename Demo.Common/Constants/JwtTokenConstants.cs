﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Common.Constants {
 public   class JwtTokenConstants {
     public const string TOKEN_KEY_NAME = "TokenKey";
     public const string TOKEN_VALID_ISSUER = "DemoIssuer";
     public const string TOKEN_VALID_AUDIENCE = "DemoAudience";
     public const string USER_ID_CLAIM = "user_id";
     public const string USER_FIRST_NAME_CLAIM = "user_full_names";
     public const string USER_FAMILY_NAME_CLAIM = "user_full_names";
    }
}
