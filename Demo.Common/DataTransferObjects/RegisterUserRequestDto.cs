﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace Demo.Common.DataTransferObjects {
    public class RegisterUserRequestDto {
        [Required(ErrorMessage = "Username is required")]
        //[StringLength(16, ErrorMessage = "Username should not be more than 16 characters")]
        [JsonPropertyName("username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [JsonPropertyName("firstName")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Family name is required")]
        [JsonPropertyName("familyName")]
        public string FamilyName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}
