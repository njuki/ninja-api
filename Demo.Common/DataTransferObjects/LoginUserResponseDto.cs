﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Demo.Common.DataTransferObjects {
    public class LoginUserResponseDto {

        [JsonPropertyName("jwtToken")]
        public string JwtToken { get; set; }

        [JsonPropertyName("errorMessage")]
        public string ErrorMessage { get; set; }

    }
}
