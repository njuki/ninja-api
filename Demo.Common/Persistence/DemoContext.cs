﻿using Demo.Common.Entities;
using Microsoft.EntityFrameworkCore;

namespace Demo.Common.Persistence {
    public class DemoContext : DbContext {

        public DemoContext(DbContextOptions<DemoContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
        }

        public DbSet<User> Users { get; set; }
    }


}