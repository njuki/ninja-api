﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Demo.Common.Entities {
    public class User {

        [Required, Key]
        public string Username { get; set; }

        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string FamilyName { get; set; }

        [Required]
        public string PasswordHash { get; set; }
    }
}
