## Ninja App
Is a simple api that allows one to register users and have them login, returning a jwt token on each request.
Data is stored in-memory and will not persist across sessions.

## Guide to the api endpoints
The api has four endpoints, two for authentication (/user/register and /user/login), and two additional ones for authorization (/information/free and /information/protected)

##### Testing App
A simple angular app to test the api is available [here](https://bitbucket.org/njuki/ninja-app/src/master/)

#### Register
#### http://localhost/user/register | POST

sample request body:
```json
{
"username": "john",
"firstName": "John",
"familyName": "Deere",
"password": "1234"
}
```

On success it returns: `200 OK`

and a token that looks like this:

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiamRqbCIsInVzZXJfZnVsbF9uYW1lcyI6IkpvaG4gRGVlcmUiLCJpYXQiOjE1ODIyMDYzMjUsIm5iZiI6MTU4MjIwNTcyNSwiZXhwIjoxNTgyMjA2MDI1LCJpc3MiOiJEZW1vSXNzdWVyIiwiYXVkIjoiRGVtb0F1ZGllbmNlIn0.pIlaNDJIgkTSoqkN1FFxBf_bpYLhJmKUZyEZwVWzqwM
```

In case of error it returns an object that looks like this:
```json
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
    "title": "One or more validation errors occurred.",
    "status": 400,
    "traceId": "|25e7ede2-4663ddffc975c8f5.",
    "errors": {
        "field": [
            "Error message"
        ]
    }
}
```

#### Login
#### http://localhost/user/login | POST

sample request body:
```json
{
"username": "john",
"password": "1234"
}
```

if successful, returns: `200 OK` plus a jwt token

in case of error it returns `400 Bad Request`
and the error message `"Invalid username or password"`


#### Unauthorized resource
#### http://localhost/information/free | GET

Always returns
`"General information that anyone can access"`


#### Protected resource
#### http://localhost/information/protected : GET

If request is authorized (has a valid token in the authorization header) it returns:
`"Protected information for certified ninjas only"`

otherwise it returns
`401 Unauthorized`